output "vpc" {
    value   =   "${aws_vpc.assigner.id}"
}

output "private-subnet-a" {
    value   =   "${aws_subnet.assigner-private-a.id}"
}

output "private-subnet-b" {
    value   =   "${aws_subnet.assigner-private-b.id}"
}

output "private-subnet-c" {
    value   =   "${aws_subnet.assigner-private-c.id}"
}

output "public-subnet-a" {
    value   =   "${aws_subnet.assigner-public-a.id}"
}

output "public-subnet-b" {
    value   =   "${aws_subnet.assigner-public-b.id}"
}

output "public-subnet-c" {
    value   =   "${aws_subnet.assigner-public-c.id}"
}
