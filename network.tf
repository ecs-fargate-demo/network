terraform {
  backend "s3" {
    key    = "poc/network/terraform.tfstate"
    bucket = "assigner-terraform-state"
    region = "ap-southeast-2"
  }
}

resource "aws_vpc" "assigner" {
  cidr_block = "10.0.0.0/16"

  tags {
    service = "production"
    type    = "infrastructure"
    owner   = "shared-service"
  }
}

resource "aws_subnet" "assigner-public-a" {
  vpc_id                  = "${aws_vpc.assigner.id}"
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "ap-southeast-2a"

  tags {
    service = "production"
    type    = "infrastructure"
    owner   = "shared-service"
  }
}

resource "aws_subnet" "assigner-public-b" {
  vpc_id                  = "${aws_vpc.assigner.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "ap-southeast-2b"

  tags {
    service = "production"
    type    = "infrastructure"
    owner   = "shared-service"
  }
}

resource "aws_subnet" "assigner-public-c" {
  vpc_id                  = "${aws_vpc.assigner.id}"
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "ap-southeast-2c"

  tags {
    service = "production"
    type    = "infrastructure"
    owner   = "shared-service"
  }
}

resource "aws_subnet" "assigner-private-a" {
  vpc_id     = "${aws_vpc.assigner.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "ap-southeast-2a"
  tags {
    service = "production"
    type    = "infrastructure"
    owner   = "shared-service"
  }
}

resource "aws_subnet" "assigner-private-b" {
  vpc_id     = "${aws_vpc.assigner.id}"
  cidr_block = "10.0.4.0/24"
  availability_zone = "ap-southeast-2b"
  tags {
    service = "production"
    type    = "infrastructure"
    owner   = "shared-service"
  }
}

resource "aws_subnet" "assigner-private-c" {
  vpc_id     = "${aws_vpc.assigner.id}"
  cidr_block = "10.0.5.0/24"
  availability_zone = "ap-southeast-2c"

  tags {
    service = "production"
    type    = "infrastructure"
    owner   = "shared-service"
  }
}

resource "aws_route_table" "assigner-private" {
  vpc_id = "${aws_vpc.assigner.id}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.assigner-ngw.id}"
  }

  tags {
    service = "production"
    type    = "infrastructure"
    owner   = "shared-service"
  }
}

resource "aws_route_table" "assigner-public" {
  vpc_id = "${aws_vpc.assigner.id}"

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = "${aws_internet_gateway.assigner-igw.id}"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.assigner-igw.id}"
  }

  tags {
    service = "production"
    type    = "infrastructure"
    owner   = "shared-service"
  }

  depends_on = ["aws_internet_gateway.assigner-igw"]
}

resource "aws_route_table_association" "rt-public-a" {
	subnet_id = "${aws_subnet.assigner-public-a.id}"
	route_table_id = "${aws_route_table.assigner-public.id}"
}

resource "aws_route_table_association" "rt-public-b" {
	subnet_id = "${aws_subnet.assigner-public-b.id}"
	route_table_id = "${aws_route_table.assigner-public.id}"
}

resource "aws_route_table_association" "rt-public-c" {
	subnet_id = "${aws_subnet.assigner-public-c.id}"
	route_table_id = "${aws_route_table.assigner-public.id}"
}

resource "aws_route_table_association" "rt-private-a" {
	subnet_id = "${aws_subnet.assigner-private-a.id}"
	route_table_id = "${aws_route_table.assigner-private.id}"
}

resource "aws_route_table_association" "rt-private-b" {
	subnet_id = "${aws_subnet.assigner-private-b.id}"
	route_table_id = "${aws_route_table.assigner-private.id}"
}

resource "aws_route_table_association" "rt-private-c" {
	subnet_id = "${aws_subnet.assigner-private-c.id}"
	route_table_id = "${aws_route_table.assigner-private.id}"
}


resource "aws_network_acl" "assigner-private" {
  vpc_id     = "${aws_vpc.assigner.id}"
  subnet_ids = ["${aws_subnet.assigner-private-a.id}", "${aws_subnet.assigner-private-b.id}", "${aws_subnet.assigner-private-c.id}"]

  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  }

  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  }

  egress {
    protocol   = "tcp"
    rule_no    = 300
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

  egress {
    protocol   = "tcp"
    rule_no    = 400
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 3389
    to_port    = 3389
  }

  egress {
    protocol   = "tcp"
    rule_no    = 500
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65335
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "10.0.0.0/16"
    from_port  = 80
    to_port    = 80
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "10.0.0.0/16"
    from_port  = 443
    to_port    = 443
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 300
    action     = "allow"
    cidr_block = "10.0.0.0/16"
    from_port  = 22
    to_port    = 22
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 400
    action     = "allow"
    cidr_block = "10.0.0.0/16"
    from_port  = 3389
    to_port    = 3389
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 500
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65335
  }

  depends_on = ["aws_nat_gateway.assigner-ngw"]
}

resource "aws_network_acl" "assigner-public" {
  vpc_id     = "${aws_vpc.assigner.id}"
  subnet_ids = ["${aws_subnet.assigner-public-a.id}", "${aws_subnet.assigner-public-b.id}", "${aws_subnet.assigner-public-c.id}"]

  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  }

  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  }

  egress {
    protocol   = "tcp"
    rule_no    = 300
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 3389
    to_port    = 3389
  }

  egress {
    protocol   = "tcp"
    rule_no    = 400
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

  egress {
    protocol   = "tcp"
    rule_no    = 500
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65335
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 300
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 3389
    to_port    = 3389
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 400
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 500
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65335
  }
}

resource "aws_nat_gateway" "assigner-ngw" {
  allocation_id = "${aws_eip.nat_eip.id}"
  subnet_id     = "${aws_subnet.assigner-public-a.id}"

  depends_on = ["aws_eip.nat_eip"]
}

resource "aws_eip" "nat_eip" {
  vpc = true
}

resource "aws_internet_gateway" "assigner-igw" {
  vpc_id = "${aws_vpc.assigner.id}"
}
