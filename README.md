# Introduction

This repository consists of terraform files for setting up complete network resources inside an AWS Account.

## Network Design
The network resources include :
* VPC (1)
* Subnets
    * Private (3 - 1 for each AZ)
    * Public  (3 - 1 for each AZ)
* Route Tables
    * Private
    * Public
* IGW
* NAT GW
* Network ACLs
    * Private
    * Public

## Examples
To add any resources (ex. NAT GW) first branch out and populate the [network.tf](https://gitlab.com/ecs-fargate-demo/network/blob/master/network.tf)

For references to the resources go to [terraform-link](https://www.terraform.io/docs/providers/aws/d/vpcs.html)

# Coveats
* Deploy is a manual step that runs only on master branch
* To make changes in the existing setup you should branch out and make changes and then perform the merge.
* To add more asstes to be used in other plans you should add the resources to the outputs.tf

